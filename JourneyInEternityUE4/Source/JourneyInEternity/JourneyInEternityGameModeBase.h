// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JourneyInEternityGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JOURNEYINETERNITY_API AJourneyInEternityGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
