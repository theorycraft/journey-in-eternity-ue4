#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PC.generated.h"

UCLASS()
class JOURNEYINETERNITY_API APC : public APlayerController
{
	GENERATED_BODY()
protected:
    virtual void BeginPlay() override;
};
