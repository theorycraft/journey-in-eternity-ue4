// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Entity.h"
#include "Base.h"
#include "GridEntity.generated.h"

/**
 * 
 */
UCLASS()
class JOURNEYINETERNITY_API AGridEntity : public AEntity
{
	GENERATED_BODY()
protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, ReplicatedUsing = onPosChanged) FPos pos;
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;
	UFUNCTION() void onPosChanged();
	UFUNCTION(BlueprintPure) FPos getPos() const;
};
