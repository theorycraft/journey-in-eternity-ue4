#pragma once
#include "GameFramework/Actor.h"
#include "Entity.generated.h"

class UAbilitySystemComponent;

UCLASS()
class JOURNEYINETERNITY_API AEntity : public APawn
{
	GENERATED_BODY()
	
public:	
	AEntity();

protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Replicated) UAbilitySystemComponent* abilitysys;
	virtual void BeginPlay() override;

public:	
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;
	virtual void Tick(float DeltaTime) override;

};
