#pragma once

#include "CoreMinimal.h"
#include "Base.generated.h"

//prints
#define DEBUG_TEXT_TIME 10
#define printint(x) if(GEngine) { FString fstr = FString::FromInt(x); GEngine->AddOnScreenDebugMessage(-1, DEBUG_TEXT_TIME, FColor::Purple, *fstr); }
#define printbool(x) if(GEngine) { GEngine->AddOnScreenDebugMessage(-1, DEBUG_TEXT_TIME, FColor::Purple, TEXT(x)); }
#define printstr(x) if(GEngine) { GEngine->AddOnScreenDebugMessage(-1, DEBUG_TEXT_TIME, FColor::Purple, x); }
#define printfstr(x) if(GEngine) { GEngine->AddOnScreenDebugMessage(-1, DEBUG_TEXT_TIME, FColor::Purple, x? TEXT("true") : TEXT("false")); }
#define printfl(x) if(GEngine) { FString fstr = FString::SanitizeFloat(x); GEngine->AddOnScreenDebugMessage(-1, DEBUG_TEXT_TIME, FColor::Purple, *fstr); }
#define printvec(x) if(GEngine) { FString fstr = x.ToString(); GEngine->AddOnScreenDebugMessage(-1, DEBUG_TEXT_TIME, FColor::Purple, *fstr); }
#define printpos(a) if(GEngine) { FString fstr = TEXT("(") + FString::FromInt(a.x) + TEXT(", ") + FString::FromInt(a.y) + TEXT(")"); GEngine->AddOnScreenDebugMessage(-1, DEBUG_TEXT_TIME, FColor::Purple, *fstr); }

UENUM(BlueprintType)
enum class HexDirection : uint8 {
	Right, DownRight, DownLeft, Left, UpLeft, UpRight
};

USTRUCT(BlueprintType)
struct FPos {
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 x;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 y;
	FPos();
	FPos(const int32& _x, const int32& _y);
	FPos(const FVector& vec);
	bool operator==(const FPos& other) const;
	FPos& operator+=(const FPos& other);
	FPos& operator-=(const FPos& other);
	FPos& operator*=(const int32& mult);
	FPos& operator/=(const int32& mult);
	operator FString() const;
	FVector toVec(int32 _z = 0) const;
	int32 z() const;
	friend uint32 GetTypeHash(const FPos& other) {
		FVector2D v(other.x, other.y);
		return GetTypeHash(v);
	}
	friend FPos operator+(const FPos& first, const FPos& second) {
		return FPos(first.x + second.x, first.y + second.y);
	}
	friend FPos operator-(const FPos& first, const FPos& second) {
		return FPos(first.x - second.x, first.y - second.y);
	}
	friend FPos operator*(const FPos& first, const int32& second) {
		return FPos(first.x * second, first.y * second);
	}
	friend FPos operator/(const FPos& first, const int32& second) {
		return FPos(first.x / second, first.y / second);
	}
};

namespace Config {
	const float side = 100.0f;
	const float cornerToCorner = side * 2.0f;
	const float halfSide = side / 2.0f;
	const float sideToSide = FMath::Sqrt(3) * side;
	const float centerToSide = sideToSide / 2.0f;
	const float hexX = centerToSide + halfSide;
	const FPos invalid = FPos(-1, -1);
}