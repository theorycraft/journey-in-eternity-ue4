#pragma once
#include "Base.h"
#include "Entity.h"
#include "PaperSpriteComponent.h"
#include "Grid.generated.h"

struct FPos;

UENUM(BlueprintType)
enum class TileType : uint8 
{
	None, Grass, WoodenFloor
};

USTRUCT(BlueprintType)
struct FTile
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TileType type;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) AEntity* entity;
	FTile(const TileType t = TileType::None, AEntity* e = nullptr) :
		type(t), entity(e)
	{

	}
};

UCLASS()
class JOURNEYINETERNITY_API AGrid : public AEntity
{
	GENERATED_BODY()
protected:
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;
	UPROPERTY(BlueprintReadOnly, EditAnywhere) uint8 x;
	UPROPERTY(BlueprintReadOnly, EditAnywhere) uint8 y;
	UPROPERTY(BlueprintReadOnly, EditAnywhere) UPaperSpriteComponent* sprite;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Replicated) TArray<FTile> tiles;
	UFUNCTION(BlueprintCallable) void reset();

public:
	AGrid();
	~AGrid();
	const FTile* getTile(const FPos& pos) const;
	UFUNCTION(BlueprintCallable) bool isValid(const FPos& pos) const;
	UFUNCTION(BlueprintCallable) FVector toWorldLocation(const FPos& pos) const;
};
