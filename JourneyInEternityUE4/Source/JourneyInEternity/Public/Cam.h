#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Cam.generated.h"

UCLASS()
class JOURNEYINETERNITY_API ACam : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACam();

protected:
	virtual void BeginPlay() override;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float movementSpeed;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION(BlueprintCallable) void move(const float& deltaTime, const float& forward, const float& back, const float& left, const float& right);
};
