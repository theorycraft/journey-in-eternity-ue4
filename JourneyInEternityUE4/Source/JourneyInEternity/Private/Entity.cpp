#include "Entity.h"
#include "AbilitySystemComponent.h"
#include "Net/UnrealNetwork.h"
#include "Base.h"

AEntity::AEntity()
{
	bReplicates = true;
	SetReplicateMovement(true);
	bAlwaysRelevant = true;
	bNetLoadOnClient = true;
	abilitysys = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("Ability System"));
	abilitysys->SetIsReplicated(true);
	//abilitysys->InitAbilityActorInfo(this, this);
}

void AEntity::BeginPlay()
{
	Super::BeginPlay();
}

void AEntity::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AEntity, abilitysys);
}

void AEntity::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}