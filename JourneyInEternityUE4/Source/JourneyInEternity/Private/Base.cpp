#include "Base.h"

FPos::FPos() {
	x = 0;
	y = 0;
}

FPos::FPos(const int32& _x, const int32& _y) {
	x = _x;
	y = _y;
}

FPos::FPos(const FVector& vec) 
{
	FVector converted(vec.Y / Config::hexX, vec.X / Config::sideToSide - vec.Y / (2 * Config::hexX), 0.0f);
	converted.Z = -converted.X - converted.Y;
	x = FMath::RoundHalfToZero(converted.X);
	y = FMath::RoundHalfToZero(converted.Y);
	int32 rz = FMath::RoundHalfToZero(converted.Z);
	float dx = FMath::Abs(converted.X - x);
	float dy = FMath::Abs(converted.Y - y);
	float dz = FMath::Abs(converted.Z - rz);
	if (dx > dy && dx > dz) x = -y - rz;
	else if (dy > dz) y = -x - rz;
}

bool FPos::operator==(const FPos& other) const {
	return x == other.x && y == other.y;
}

FPos& FPos::operator+=(const FPos& other) {
	x += other.x;
	y += other.y;
	return *this;
}

FPos& FPos::operator-=(const FPos& other) {
	x -= other.x;
	y -= other.y;
	return *this;
}

FPos& FPos::operator*=(const int32& mult) {
	x *= mult;
	y *= mult;
	return *this;
}

FPos& FPos::operator/=(const int32& mult) {
	x /= mult;
	y /= mult;
	return *this;
}

FPos::operator FString() const {
	return TEXT("(") + FString::FromInt(x) + TEXT(", ") + FString::FromInt(y) + TEXT(")");
}

FVector FPos::toVec(int32 _z) const {
	return FVector(y * Config::sideToSide + x * Config::centerToSide, x * Config::hexX, _z);
}

int32 FPos::z() const {
	return -x - y;
}