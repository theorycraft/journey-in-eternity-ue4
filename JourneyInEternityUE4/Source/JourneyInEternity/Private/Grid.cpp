#include "Grid.h"
#include "Net/UnrealNetwork.h"
#include "Base.h"

AGrid::AGrid()
{
	bReplicates = true;
	bAlwaysRelevant = true;
	bNetLoadOnClient = true;
	sprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Sprite"));
}

AGrid::~AGrid()
{
}

void AGrid::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AGrid, tiles);
}

void AGrid::reset() {
	tiles.SetNumZeroed(x * y + y, true);
}

const FTile* AGrid::getTile(const FPos& pos) const {
	if (!isValid(pos)) return nullptr;
	return &tiles[pos.x * y + pos.y];
}

bool AGrid::isValid(const FPos& pos) const {
	return pos.x >= 0 && pos.y >= 0 && pos.x < x && pos.y < y;
}

FVector AGrid::toWorldLocation(const FPos& pos) const
{
	return GetActorLocation() + pos.toVec();
}
