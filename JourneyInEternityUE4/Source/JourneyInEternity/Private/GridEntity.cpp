#include "GridEntity.h"
#include "Net/UnrealNetwork.h"

void AGridEntity::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AGridEntity, pos);
}

void AGridEntity::onPosChanged() {
	SetActorLocation(pos.toVec());
}

FPos AGridEntity::getPos() const {
	return pos;
}