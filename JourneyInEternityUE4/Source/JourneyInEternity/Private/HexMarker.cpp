#include "HexMarker.h"

AHexMarker::AHexMarker()
{
	PrimaryActorTick.bCanEverTick = false;

}

void AHexMarker::BeginPlay()
{
	Super::BeginPlay();

}

void AHexMarker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHexMarker::setPosFromVector(const FVector& vec) {
	setPos(FPos(vec));
}

void AHexMarker::setProperties(const FPos& pos, MarkerType type)
{
	setPos(pos);
	setType(type);
}

void AHexMarker::setPos(const FPos& pos)
{
	ETeleportType tele = ETeleportType::TeleportPhysics;
	float f = markerType == MarkerType::Cursor ? 0.02f : 0.01f;
	SetActorLocation(pos.toVec(f), false, nullptr, tele);
}
