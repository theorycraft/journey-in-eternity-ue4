// Copyright Epic Games, Inc. All Rights Reserved.

#include "JourneyInEternity.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JourneyInEternity, "JourneyInEternity" );
